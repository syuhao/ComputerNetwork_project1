#pragma once
#include<iostream>
#include<opencv2/opencv.hpp>
#include<opencv2\imgproc\types_c.h>
#include<vector>
#include<strstream>
#include<fstream>
#include<stdio.h>
#include<io.h>
using namespace std;
using namespace cv;

namespace transformer {

    //将元组内各类图片转化为视频
    void Pic_T_Video(vector<Mat>& src_ims);

    //将视频逐帧提取进入元组成为Mat类对象
    void Video_T_Pic(string path, vector<Mat>& src_ims);

    //用于播放视频内容，参数为当目录下文件名字
    void VideoShow(string path);
}
