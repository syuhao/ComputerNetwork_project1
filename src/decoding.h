#pragma once
#include<iostream>
#include<opencv2/opencv.hpp>
#include<math.h>
#include<opencv2/imgproc/types_c.h>
#include<vector>
#include<strstream>
#include<fstream>
#include <bitset>
using namespace std;
using namespace cv;
namespace decoding
{
	const int PIXEL_SIZE = 8;//一个像素块占64个像素点
	const int LENGTH = 1000 / PIXEL_SIZE;//一个行占几个像素块
	const int PIXEL_VALUE = LENGTH * LENGTH - 2;//为图片的标号色素提供位置，因为这里我们使用的是八进制，八个色块
	//两个个八进制数 可以表示0-63 这里姑且认为我们的图片个数不会超过这个数值；之后可以再做调整
	const int FRAME_MAX = 75;//最大张数；这里如果图片播放间隔是300ms*15 大概是4.5s满足要求 之后可以调整图片的播放间隔以及张数 从而最大化信息量

	void my_decoding(const vector<Mat>& rec);
	string decode(const Mat& img);
	string print_0123(Mat img, int x, int y);
	double whiteGrayscale_percent(Mat img, int x, int y);
	double greenGrayscale_percent(Mat img, int x, int y);
	double redGrayscale_percent(Mat img, int x, int y);
	double blackGrayscale_percent(Mat img, int x, int y);
}