#include"transformer.h"

namespace transformer {

    //将图片数组转为视频
    void Pic_T_Video(vector<Mat>& src_ims)
    {
        string name;
        cout << "输入编码后的视频文件名(eg.test001.avi，需要有数字编号):" << endl;
        cin >> name;

        int frame_rate;
        cout << "输入帧率:" << endl;
        cin >> frame_rate;

        VideoWriter video(name, VideoWriter::fourcc('m', 'p', '4', 'v'), frame_rate, Size(1200, 1200), true);
        for (size_t i = 0; i < src_ims.size(); i++)
        {
            Mat image = src_ims[i].clone();
            //imshow("image", image);
            //waitKey(1000);
            //图片写入视频
            video.write(image);
        }
        VideoShow(name);
    }

    //将视频逐帧提取进入元组成为Mat类对象
    void Video_T_Pic(string path, vector<Mat>& src_ims)
    {
        VideoCapture capture(path);
        Mat frame;
        while (true) {
            int i = 0;
            //一帧一帧读
            capture >> frame;
            if (frame.empty()) {
                break;
            }
            else
            {
                src_ims.push_back(frame.clone());

            }
        }
    }

    //用于播放视频内容，参数为当目录下文件名字
    void VideoShow(string path)
    {
        VideoCapture capture(path);
        while (1)
        {
            //frame存储每一帧图像
            Mat frame;
            //读取当前帧
            capture >> frame;
            //播放完退出
            if (frame.empty()) {
                printf("播放完成\n");
                break;
            }
            //显示当前视频
            imshow("读取视频", frame);
            //延时300ms
            waitKey(300);
        }
    }
}