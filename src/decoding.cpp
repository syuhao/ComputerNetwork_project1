#include"decoding.h"

namespace decoding
{
    //获取每一个信息块内黑白比例
    double whiteGrayscale_percent(Mat img, int x, int y) {
        int white = 0;
        for (int i = x; i < x + PIXEL_SIZE; i++) {
            for (int j = 3 * y; j < 3 * (y + PIXEL_SIZE); j += 3) {
                if (img.ptr<uchar>(i)[j] >= 140 && img.ptr<uchar>(i)[j + 1] >= 140 && img.ptr<uchar>(i)[j + 2] >= 140) white++;
            }
        }
        return white / ((double)PIXEL_SIZE * PIXEL_SIZE);
    }
    double greenGrayscale_percent(Mat img, int x, int y) {
        int green = 0;
        for (int i = x; i < x + PIXEL_SIZE; i++) {
            for (int j = 3 * y; j < 3 * (y + PIXEL_SIZE); j += 3) {
                if (img.ptr<uchar>(i)[j] < 80 && img.ptr<uchar>(i)[j + 1] >= 140 && img.ptr<uchar>(i)[j + 2] < 80) green++;
            }
        }
        return green / ((double)PIXEL_SIZE * PIXEL_SIZE);
    }
    double redGrayscale_percent(Mat img, int x, int y) {
        int red = 0;
        for (int i = x; i < x + PIXEL_SIZE; i++) {
            for (int j = 3 * y; j < 3 * (y + PIXEL_SIZE); j += 3) {
                if (img.ptr<uchar>(i)[j] < 80 && img.ptr<uchar>(i)[j + 1] < 80 && img.ptr<uchar>(i)[j + 2] >= 140) red++;
            }
        }
        return red / ((double)PIXEL_SIZE * PIXEL_SIZE);
    }
    double blackGrayscale_percent(Mat img, int x, int y) {
        int black = 0;
        for (int i = x; i < x + PIXEL_SIZE; i++) {
            for (int j = 3 * y; j < 3 * (y + PIXEL_SIZE); j += 3) {
                if (img.ptr<uchar>(i)[j] < 80 && img.ptr<uchar>(i)[j + 1] < 80 && img.ptr<uchar>(i)[j + 2] < 80) black++;
            }
        }
        return black / ((double)PIXEL_SIZE * PIXEL_SIZE);
    }
    //对图片内像素块进行遍历，若黑/白数量超过一定阈值，便判定为黑/白，否则为误码
    string print_0123(Mat img, int x, int y) {
        string s = "";
        int tag = 0;
        for (int i = x; i < x + 992; i += PIXEL_SIZE) {
            for (int j = y; j < y + 992; j += PIXEL_SIZE) {
                double whiteRate = whiteGrayscale_percent(img, i, j);
                double greenRate = greenGrayscale_percent(img, i, j);
                double redRate = redGrayscale_percent(img, i, j);
                double blackRate = blackGrayscale_percent(img, i, j);
                double maxum = 0, maxum2 = 0;
                if (whiteRate > maxum)
                {
                    maxum2 = maxum;
                    maxum = whiteRate;
                    tag = 1;
                }
                if (greenRate > maxum)
                {
                    maxum2 = maxum;
                    maxum = greenRate;
                    tag = 3;
                }
                if (redRate > maxum)
                {
                    maxum2 = maxum;
                    maxum = redRate;
                    tag = 2;
                }
                if (blackRate > maxum)
                {
                    maxum2 = maxum;
                    maxum = blackRate;
                    tag = 0;
                }
                if (abs(maxum - maxum2) < 0.01) s += '-';
                else if (tag == 0) s += '0';
                else if (tag == 1) s += '1';
                else if (tag == 2) s += '2';
                else s += '3';
            }
        }
        return s;
    }

    //单张图片解码
    string decode(const Mat& img) {
        string rt = print_0123(img, 14, 14);
        return rt;
    }
    //接口
    void my_decoding(const vector<Mat>& rec) {

        string path2 = "1.bin", path3 = "v1.bin";

        cout << "请输入信息文件名称" << endl;
        cin >> path2;
        cout << "请输入校验文件名称" << endl;
        cin >> path3;

        fstream file(path2, ios::binary | ios::out);
        fstream vfile(path3, ios::binary | ios::out);

        //记录每个标号的图片是否被读取过
        int used[5000] = { 0 };

        //从第一张图片开始读取
        int pic_tag = 1;
        //从第一张图片开始读取

        for (int i = 0; i < rec.size(); i++) {

            //读取图片中的0-5串

            string s = decoding::decode(rec[i]);
            int num = 0;
            //获得图片标号信息
            for (int j = 0; j < s.size() && j < 8; j++) {
                num *= 2;
                if (s[j] == '0') num += 0;
                else if (s[j] == '1') num += 1;
                else if (s[j] == '-') { num = 10000; break; }
            }
            //
            //如果读取图片标号不正确
           if (num != pic_tag)continue;

            //选取每个标号的第二张图片
            //if (!used[num]) { used[num]++; continue; };

            //读取下一张图片
            pic_tag++;
            //选取每个标号的第二张图片
            cout << s << endl;
            cout << endl;
            //读取下一张图片
      
            for (int j = 8; j < s.size(); j += 4) {
                unsigned char ch = 0, lg = 0;
                for (int k = j + 3; k >= j; k--)
                {
                    if (s[k] == '-')
                    {
                        ch = '-';
                        break;
                    }
                    ch += (s[k] - 48) * pow(4, k - j);
                }
                file.write((char*)&ch, sizeof(ch));
            }

        }
    }

}