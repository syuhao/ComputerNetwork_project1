#include"encoding.h"
#include"transformer.h"

namespace encoding {

	string Get_String()
	{
		string path = "";
		string rt;
		cout << "请输入您的二进制文件的名称:" << endl;
		cin >> path;
		fstream file(path, ios::binary | ios::in);//二进制读取格式，如果测试时没有二进制文件的话，测试时可以调整
		if (!file)
		{
			cout << "打开文件失败" << endl;
			return rt;
		}
		unsigned char ch;
		while (file.read((char*)&ch, sizeof(ch)) && rt.size() < (PIXEL_VALUE * FRAME_MAX) / 4)
		{
			rt += ch;
		}
		//因为这里每个字符用四个色块表示，三个八进制为可以表示0-511完全足够
		return rt;
	}

	string String_T_Octal(string s)//名先不改了 八进制而已
	{
		string rt;//存放4进制字符串
		int s_length = s.length();
		for (int i = 0; i < s_length; i++)
		{
			unsigned char ch = s[i];
			for (int i = 0; i < 4; i++)
				//注意这里是倒着存的比如十进制16 我这里存的是020 所以到时候解码部分可能会做调整
				//存三个是为了一致性
			{
				rt += (ch % 4 + 48);
				ch /= 4;
			}
		}
		return rt;
	}

	vector<string> Split(string s)
	{
		vector<string> rt;
		int len = s.length();
		int head = 0, end = min(head + PIXEL_VALUE, len);
		while (head != len)
		{
			string tmp;
			for (int i = head; i < end; i++)
			{
				tmp.push_back(s[i]);
			}
			rt.push_back(tmp);
			head = end;
			end = min(head + PIXEL_VALUE, len);
		}

		return rt;
	}

	Mat String_T_Mat(string s, int num)
	{
		Mat img(LENGTH, LENGTH, CV_8UC3, Scalar(0, 0, 0));//8uc3代表是rgb图像，000纯黑初始化
		for (int i = 23; i >= 0; i -= 3)
			//存标号
		{
			int value = num % 2;
			num /= 2;
			switch (value)
			{
			case 0:
				img.ptr<uchar>(0)[i] = 0;
				img.ptr<uchar>(0)[i - 1] = 0;
				img.ptr<uchar>(0)[i - 2] = 0;//黑色色块
				break;
			case 1://白色
				img.ptr<uchar>(0)[i] = 255;
				img.ptr<uchar>(0)[i - 1] = 255;
				img.ptr<uchar>(0)[i - 2] = 255;
				break;
	
			}
		}
		int cnt = 0, len = s.length();
		for (int i = 0; i < LENGTH; i++)
		{
			for (int j = 0; j < 3 * LENGTH; j = j + 3)
			{
				if (i == 0 && j < 24) continue;
				if (cnt >= len) continue;//因为初始化的时候就是全黑的
				else
				{
					char c = s[cnt++];
					switch (c)
					{
					case '0'://黑色色块
						img.ptr<uchar>(i)[j] = 0;
						img.ptr<uchar>(i)[j + 1] = 0;
						img.ptr<uchar>(i)[j + 2] = 0;
						break;
					case '1'://白色
						img.ptr<uchar>(i)[j] = 255;
						img.ptr<uchar>(i)[j + 1] = 255;
						img.ptr<uchar>(i)[j + 2] = 255;
						break;
					case '2'://红色 
						img.ptr<uchar>(i)[j + 2] = 255;
						img.ptr<uchar>(i)[j] = 0;
						img.ptr<uchar>(i)[j + 1] = 0;
						break;
					case '3'://绿色
						img.ptr<uchar>(i)[j + 1] = 255;
						img.ptr<uchar>(i)[j] = 0;
						img.ptr<uchar>(i)[j + 2] = 0;
						break;
					}
				}
			}
		}

		//存入图片
		Mat bigger_img(992, 992, CV_8UC3, Scalar(0, 0, 0));//放大

		for (int i = 0; i < 992;i++)
		{
			for (int j = 0; j < 2976; j += 3)//三通道放大要重新编码
			{
				int x = i / PIXEL_SIZE;//不是简单缩放
				int y = j / (3 * PIXEL_SIZE);
				y = y * 3;
				bigger_img.ptr<uchar>(i)[j] = img.ptr<uchar>(x)[y];
				bigger_img.ptr<uchar>(i)[j + 1] = img.ptr<uchar>(x)[y + 1];
				bigger_img.ptr<uchar>(i)[j + 2] = img.ptr<uchar>(x)[y + 2];
			}
		}
		//添加轮廓
		Mat rt(1200, 1200, CV_8UC3, Scalar(0, 0, 0));//最后的大小

		for (int i = 0; i < 1200; i++)
		{
			for (int j = 0; j < 3600; j += 3)
			{
				rt.ptr<uchar>(i)[j] = 255;//白边
				rt.ptr<uchar>(i)[j + 1] = 255;
				rt.ptr<uchar>(i)[j + 2] = 255;
				if (i <= 1125 && i >= 75 && j <= 3375 && j >= 225)
				{
					rt.ptr<uchar>(i)[j] = 0;//黑框
					rt.ptr<uchar>(i)[j + 1] = 0;//黑框
					rt.ptr<uchar>(i)[j + 2] = 0;//黑框
				}
				if (i <= 1110 && i >= 90 && j <= 3330 && j >= 270)
				{
					rt.ptr<uchar>(i)[j] = 255;//内白边
					rt.ptr<uchar>(i)[j + 1] = 255;//内白边
					rt.ptr<uchar>(i)[j + 2] = 255;//内白边
				}
			}
		}

		//存入信息
		for (int i = 104; i < 1096; i++)
			for (int j = 312; j < 3288; j += 3)
			{
				rt.ptr<uchar>(i)[j] = bigger_img.ptr<uchar>(i - 104)[j - 312];
				rt.ptr<uchar>(i)[j + 1] = bigger_img.ptr<uchar>(i - 104)[j + 1 - 312];
				rt.ptr<uchar>(i)[j + 2] = bigger_img.ptr<uchar>(i - 104)[j + 2 - 312];
			}

		return rt;
	}


	vector<Mat> String_T_Vmat(vector<string> s)//划分好的八进制串
	{
		int len = s.size();//串的数量

		//限制
		len = min(len, FRAME_MAX);
		vector<Mat> rt;//图像数组
		//for (int k = 0; k < 2; k++)
		//{
		//	Mat frame(1080, 1080, CV_8UC3, Scalar(255, 255, 255));
		//	for (int i = 0; i < 1080; i++)
		//	{
		//		for (int j = 0; j < 3240; j += 3)
		//		{
		//			frame.ptr<uchar>(i)[j] = 255;//白边
		//			frame.ptr<uchar>(i)[j + 1] = 255;
		//			frame.ptr<uchar>(i)[j + 2] = 255;
		//			if (i <= 1045 && i >= 35 && j <= 3135 && j >= 105)
		//			{
		//				frame.ptr<uchar>(i)[j] = 0;//黑框
		//				frame.ptr<uchar>(i)[j + 1] = 0;//黑框
		//				frame.ptr<uchar>(i)[j + 2] = 0;//黑框
		//			}
		//			if (i <= 1030 && i >= 50 && j <= 3090 && j >= 150)
		//			{
		//				frame.ptr<uchar>(i)[j] = 255;//内白边
		//				frame.ptr<uchar>(i)[j + 1] = 255;//内白边
		//				frame.ptr<uchar>(i)[j + 2] = 255;//内白边
		//			}
		//		}
		//	}
		//	rt.push_back(frame);
		//}
		for (int i = 0; i < len; i++) {
			Mat tmp = String_T_Mat(s[i], i + 1);
			rt.push_back(tmp);
		}
		return rt;
	}
}