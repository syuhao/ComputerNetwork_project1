#pragma once
#include<iostream>
#include<opencv2/opencv.hpp>
#include<math.h>
#include<opencv2/imgproc/types_c.h>
#include<vector>
#include<strstream>
#include<fstream>
using namespace std;
using namespace cv;

namespace encoding
{
	const int PIXEL_SIZE = 8;//一个像素块占64个像素点
	const int LENGTH =992 / PIXEL_SIZE;//一个行占几个像素块
	const int PIXEL_VALUE = LENGTH * LENGTH - 8;//为图片的标号色素提供位置，因为这里我们使用的是八进制，八个色块
	//两个个八进制数 可以表示0-63 这里姑且认为我们的图片个数不会超过这个数值 ；之后可以再做调整
	const int FRAME_MAX = 75;//最大张数；这里如果图片播放间隔是300ms*15 大概是4.5s满足要求 之后可以调整图片的播放间隔以及张数 从而最大化信息量

	string Get_String();//获得文件当中的字符串
	string String_T_Octal(string s);//字符串转化为八进制串
	vector<string> Split(string s);//八进制串分别划分到不同的图中
	Mat String_T_Mat(string s, int num);//串转化为图
	vector<Mat> String_T_Vmat(vector<string> s);//将串转化为图 然后存入vector<Mat>
}

//0代表黑 1白 2红 3黄 4绿 5蓝 6 靛 7紫
//0黑  255白 红76.245 绿149.685
//0-50归为黑  51-120归为红 121-180归为绿 180-255归为白 